+++
title = "services"
+++

here is some public services i'm providing:

* [b](https://b.c0de.in): minimalist private**b**in where the server has zer0 knowledge of pasted data
* [f](https://f.c0de.in): **f**ilebrowser to share and store stuff
* [m](https://m.c0de.in): listen freely my entire **m**usic discography with navidrome
* [r](https://r.c0de.in): privacy focused **r**eddit frontend using libreddit
* [t](https://t.c0de.in): **t**emporary ro fileserver
* [v](https://v.c0de.in): watch **v**ideos with jellyfin
