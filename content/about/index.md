+++
title = "me"
+++

my name is valentin maillot, val or valoche. i am currently living in elsass, france.

for a living i am a system engineer, passionate about free software and GNU/Linux, automation, microservices and high availability infrastructure management.

check out my [resume](resume.pdf) for more information.

adrenaline and extreme sports addict i love being outside and enjoying life.

# contact

* email: [vmaillot@pm.me](mailto:vmaillot@pm.me), pubkey [FEEA 886C 63D4 045E 529F 016D 025E 25EB B767 84B0](pubkey.asc)
* irc: **jolia**
  + otr `4F6DDC5C 20306856 E1CE3EA9 54157BB7 B53FAC02` [libera](https://libera.chat)
  + otr `9C4C1651 5FB3DF2E 673A6E47 E661D576 F37F1F57` [efnet](http://www.efnet.org)
  + [unix.chat](https://unix.chat)
* linkedin: [me](https://www.linkedin.com/in/333b14137)
