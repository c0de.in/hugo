+++
title = "ring"
+++

friends, inspiration & communities on the web:

* [aomame](https://aomame.neocities.org)
* [arcaik](https://arcaik.net)
* [dcat](https://lyngvaer.no)
* [dustri](https://dustri.org)
* [drkhsh](https://drkhsh.at)
* [eyenx](https://eyenx.ch)
* [iotek](https://iotek.org)
* [mort](https://mort.coffee)
* [movq](https://www.uninformativ.de)
* [neeasade](https://neeasade.net)
* [nixers](https://nixers.net)
* [ols](https://ols.wtf)
* [pyratebeard](https://pyratebeard.net)
* [thugcrowd](https://thugcrowd.com)
* [venam](https://venam.nixers.net)
* [xero](https://whois.xxe.ro)
* [z3bra](https://z3bra.org)
